Создание виртуального окружения и установка зависимостей:  
>       poetry install  
>       poetry shell  

Создание бд:  
>       sudo make db  

Миграции:
>       make migrate  
 
Запуск приложения:
>       make run  

Структура проекта:  
+   app - основной код приложения,  
+   config - настройки приложения,  
+   utils - работа с бд,  
+   connection - соединение с бд,  
+   migrator - миграции,  
+   models - модели бд,  
+   .env - файл с переменными окружения,  
+   schemas - классы сущностей.  

Пример запроса:  
```
import requests
resp = requests.post("http://127.0.0.1:8000/api/v1/get_questions", json={"questions_num": 2})
print(resp.json())
```  

Пример ответа:  
`
{'question': 'Larger trees are starting to encroach north on this ecosystem defined by its lack of them', 'answer': 'the tundra'}
`
from app.endpoints.ping import api_router as app_health
from app.endpoints.get_questions import api_router as get_quest


list_of_routes = [
    app_health,
    get_quest
]


__all__ = [
    "list_of_routes"
]

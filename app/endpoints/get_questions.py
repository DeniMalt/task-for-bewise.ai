from fastapi import APIRouter, Depends, HTTPException, Body
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from app.db.connection import get_session
from app.schemas import QuestionsNum
from app.utils import get_questions

api_router = APIRouter(
    prefix="/get_questions",
    tags=["questions"]
)


@api_router.post("")
async def get_question(
    session: AsyncSession = Depends(get_session),
    questions_num: QuestionsNum = Body(..., example={"questions_num": 10})
):
    if questions_num.questions_num <= 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="invalid data(question_num must be more 0)")

    res = await get_questions(questions_num.questions_num, session)
    if res:
        return {"question": res.question, "answer": res.answer}
    else:
        return None

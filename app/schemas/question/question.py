from pydantic import BaseModel


class PreviousQuestion(BaseModel):
    question: str
    answer: str


class QuestionsNum(BaseModel):
    questions_num: int

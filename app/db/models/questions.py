from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import TEXT, TIMESTAMP

from .base import BaseTable


class Questions(BaseTable):
    __tablename__ = "questions"

    question = Column(
        "questions",
        TEXT,
        nullable=False
    )

    answer = Column(
        "answers",
        TEXT,
        nullable=False
    )

    dt_created_question = Column(
        TIMESTAMP(timezone=True),
        nullable=False,
        doc="Date and time of create (type TIMESTAMP)",
    )

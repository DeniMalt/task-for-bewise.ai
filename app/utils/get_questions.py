from datetime import datetime
from aiohttp import ClientSession
from sqlalchemy.ext.asyncio import AsyncSession

from app.schemas.question.question import PreviousQuestion
from app.db.models import Questions
from .get_questions_from_db.get_questions_from_db import get_questions_db


async def get_questions(count: int, session: AsyncSession) -> PreviousQuestion:
    query_get_q = await get_questions_db(session)
    result = set()
    get_questions_from_db = set(query_get_q)
    previous_question = None
    while len(result) < count:
        async with ClientSession("https://jservice.io") as client:
            async with client.get(f"/api/random?count={count}") as req:
                resp = await req.json()
                for i in range(len(resp)):
                    if resp[i]["question"] not in get_questions_from_db and len(result) < count:
                        result.add(
                            Questions(question=resp[i]["question"], answer=resp[i]["answer"],
                                      dt_created_question=datetime.strptime(resp[i]["created_at"][:-1],
                                                                            "%Y-%m-%dT%H:%M:%S.%f"))
                        )
                    if len(result) == count - 2:
                        previous_question = PreviousQuestion(question=resp[i]["question"],
                                                             answer=resp[i]["answer"])

    await session.run_sync(lambda ses: ses.bulk_save_objects(result))
    await session.commit()

    return previous_question

import sqlalchemy.engine
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.models import Questions


async def get_questions_db(session: AsyncSession) -> sqlalchemy.engine.ScalarResult:
    query = select(Questions.question)
    return await session.scalars(query)
